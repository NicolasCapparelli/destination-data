# Destination Data

A website that gives you travel information on destinations the user chooses. Also allows comparisons between multiple destinations. The main reason I started working on this was to mess around with the open source WebGL Globe.

## WebGL Globe
https://github.com/askmike/realtime-webgl-globe

# Concept
Minimal or no implementation has been done yet, or the repository is only intended to be a limited example, demo, or proof-of-concept.