let globeHome = document.getElementById('divEarth');
let urls = {
    earth: '../dependencies/realtime-webgl-globe-master/img/world.jpg',
    bump: '../dependencies/realtime-webgl-globe-master/img/bump.jpg',
    specular: '../dependencies/realtime-webgl-globe-master/img/specular.jpg'
};

// create a globe
let globe = new Globe(globeHome, urls);

// start it
globe.init();

// random data
let data = {
    color: '#FF0000',
    size: 2,
    lat: 25.08339012, // Nassau!
    lon: -77.35004378   // Nassau!
};

// add a block on Nassau
globe.addBlock(data);
globe.center(data);
globe.zoomRelative(200);
