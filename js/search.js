// Changes the first letter of every word to uppercase
// Courtesy of: https://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}


// The input box on the home page
let searchBox = document.getElementById("inputDestination");

// Creating container for suggestions
let suggestionContainer = $('#divSuggestionContainer');

// Amount of suggestions shown in the suggestion container
let suggestionAmount = 5;

// Tracks the suggestion that the use is trying to choose
let focusTracker = -1;

// Holds the last search query actually typed by the user
let queryBeforeSuggestion = "";

// Listen for key UP/DOWN to select search suggestion
searchBox.addEventListener('keydown', function (e) {

    if (suggestionContainer.css("visibility") === "visible") { // If arrow key UP is pressed
        // If arrow key DOWN is pressed
        if (e.which === 38) {
            e.preventDefault();

            // If the user has scrolled to the first suggestion, set focusTracker to the last suggestion
            if (focusTracker <= 0){
                focusTracker = suggestionAmount - 1;
            } else {
                focusTracker--
            }

        }

        // If arrow key DOWN is pressed
        else if (e.which === 40) {
            e.preventDefault();

            if (focusTracker >= suggestionAmount - 1) {
                focusTracker = 0;
            } else {
                focusTracker++
            }
        }

        // If the user presses enter
        else if (e.which === 13){
            e.preventDefault();
            focusTracker = 0;
            return false
        }

        // If the user hits escape, disregard suggestion and set the value to what they had before scrolling onto a suggestion
        else if (e.which === 27) {
            e.preventDefault();
            searchBox.value = queryBeforeSuggestion;
            return false
        }

        else {
            return false
        }

        // console.log();
        let selectedCell = $(".pSuggestionCell").eq(focusTracker);
        selectedCell.trigger("flip");
        searchBox.value = selectedCell.text();


    } else {}
});

// When the user types in the input box
searchBox.addEventListener('input', function (e) {

    // Keep the latest search query in case the user accidentally scrolls down and chooses a suggestion
    queryBeforeSuggestion = searchBox.value;


    // Array to hold search suggestions
    let s = [];

    // Checking for length of inputted value. If acceptable, search the data and append any strings that contain the search
    if(searchBox.value.length > 2){

        /** Note that the search uses toTitleCase(searchBox.value) instead of just searchBox.value*/
        CITY_COUNTRY.forEach(function (string) {

            if (string.includes(toTitleCase(searchBox.value))) {
                s.push(string)
            }
        });

        updateSearchSuggestions(s);
    }

    else {

        // Hide and clear the container
        suggestionContainer.css("visibility", "hidden");
        suggestionContainer.empty();

    }
});


function updateSearchSuggestions(suggestions) {

    if (suggestionContainer.css("visibility") === "hidden") {
        suggestionContainer.css("visibility", "visible");
    }

    // Clears suggestion container
    suggestionContainer.empty();

    // Goes through all of the suggestions and creates div for suggestionAmount of them
    for (let i = 0; i < suggestions.length; i++){

        // Limits the amount of suggestion to suggestionAmount
        if (i < suggestionAmount) {

            // Creating the div to place in the container
            let suggestionCell = "<div class='pSuggestionCell'><p>" + suggestions[i].replace(toTitleCase(searchBox.value), '<strong>' + toTitleCase(searchBox.value) + '</strong>') + "</p></div>";

            // Appending to container
            suggestionContainer.append(suggestionCell);
        } else {
            break;
        }
    }

}


// TODO: Have the suggestion change background color when selected. .focus() is not working
// TODO: For testing, have inputted shown on map
